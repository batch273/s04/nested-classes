class Student {
	constructor(name, email, grades) {
		this.name = name;
		this.email = email;
		this.gradeAve = undefined;
		this.passed = undefined;
		this.passedWithHonors = undefined;

		if(grades.length === 4){
		     if(grades.every(grade => grade >= 0 && grade <= 100)){
		         this.grades = grades;
		     } else {
		         this.grades = undefined;
		     }
		 } else {
		     this.grades = undefined;
		 }
	}

	login() {
		console.log(`${this.email} has logged in`);
		return this;
	}
	logout() {
		console.log(`${this.email} has logged out`);
		return this
	}
	listGrades(){
		console.log(`${this.name}'s quarterly grade averages are: ${this.grades}`);
		return this;
	}
	computeAve() {
		let sum = 0;
		this.grades.forEach(grade => sum = sum + grade);
		this.gradeAve = sum/4;
		return this;
	}
	willPass() {
		this.passed = this.computeAve().gradeAve >= 85 ? true : false;
		return this;
	}

	willPassWithHonors() {
		if (this.passed) {
		    if (this.gradeAve >= 90) {
		        this.passedWithHonors = true;
		    } else {
		        this.passedWithHonors = false;
		    }
		} else {
		    this.passedWithHonors = false;
		}
		return this;
	}
}


// Class to represent a section of students
class Section {

	// every Section object will be instantiated with an empty array for its students
	constructor(name) {
		this.name = name;
		this.students = [];
		this.honorStudents = undefined;
		this.honorsPercentage = undefined;
	}

	// Method for adding student to this section
	// This will take in the same arguments needed to instantiate a Student object
	addStudent(name, email, grades) {
		// a Student object will be instantiated before being pushed to the students property
		this.students.push(new Student(name, email, grades))

		// return the Section object afterwards, allowing us to chain this method
		return this;
	}

	// Method for computing how many students in the section are honor students

	countHonorStudents() {
		let count = 0;

		this.students.forEach(student => {

			if(student.computeAve().willPass().willPassWithHonors().passedWithHonors) {
				count++
			}

		})

		this.honorStudents = count;
		return this;
	}

	// Mini-Activity:
	/*
		Create a method that is a replica of computeHonorsPercentage()
			- create a property honorsPercentage with a value of undefined
			- create a computeHonorsPercentage() that will compute for the total number of honor students divided by total number of students multiplied by 100
				-the resulting value will be assigned to the property honorsPercentage
				-return the instance/object
	*/


	computeHonorsPercentage(){
		this.honorsPercentage = (this.honorStudents/this.students.length) * 100

		return this
	}
}

// const section1A = new Section('section1A');
// console.log(section1A);

// section1A.addStudent("John", "john@mail.com", [90, 90, 90, 90])
// section1A.students[0]
// section1A.students[0].computeAve()

// section1A.addStudent('John', 'john@mail.com', [89, 84, 78, 88])
// section1A.addStudent('Joe', 'joe@mail.com', [78, 82, 79, 85])
// section1A.addStudent('Jane', 'jane@mail.com', [87, 89, 91, 93])
// section1A.addStudent('Jessie', 'jessie@mail.com', [91, 89, 92, 93])
// console.log(section1A);


class Grade {
  	constructor(level) {
    	this.level = level;
    	this.sections = [];
    	this.totalStudents = 0;
    	this.totalHonorStudents = 0;
    	this.batchAveGrade = undefined;
    	this.batchMinGrade = undefined;
    	this.batchMaxGrade = undefined;
    	
  	}

  	addSection(name) {
  		const section = new Section(name);
  		this.sections.push(section);
  	
  		return this
  	}

  	countStudents() {
  		let totalStudents = 0;
  		this.sections.forEach((section) => {
    	totalStudents += section.students.length;
  		});
  		this.totalStudents = totalStudents;
  	
  		return this
	}
  	
  	countHonorStudents() {
  		let count = 0;
  		this.sections.forEach(section => {
  			section.countHonorStudents();
  			count += section.honorStudents;
  		});
  		this.totalHonorStudents = count;
  		return this;
  	}

  	computeBatchAveGrade() {
  		let sum = 0;
  		let count = 0;
  		this.sections.forEach(section => {
  			section.students.forEach(student => {
  				if(student.computeAve().willPass()) {
  					sum += student.gradeAve;
  					count++
  				}
  			})
  		})
  		this.batchAveGrade = sum/count;
  		return this;
  	}

  	computeBatchMinGrade() {
  		    let batchGrades = [];
  		    this.sections.forEach(section => {
  		      	section.students.forEach(student => {
  		      		for (let i = 0; i < student.grades.length; i++){
  		        		batchGrades.push(student.grades[i]);
  		        	}
  		      	});
  		    });
  		    this.batchMinGrade = Math.min.apply(Math, batchGrades);
  		    return this;
  		}

  	computeBatchMaxGrade() {
  		    let batchGrades = [];
  		    this.sections.forEach(section => {
  		      	section.students.forEach(student => {
  		      		for (let i = 0; i < student.grades.length; i++){
  		        		batchGrades.push(student.grades[i]);
  		        	}
  		      	});
  		    });
  		    this.batchMaxGrade = Math.max.apply(Math, batchGrades);
  		    return this;
  		}   




	}

	const grade1 = new Grade(1);
	console.log(grade1)

	//-----------for testing--------------//
//instantiate new Grade object
// const grade1 = new Grade(1);

// 2. add sections to this grade level
grade1.addSection('section1A');
grade1.addSection('section1B');
grade1.addSection('section1C');
grade1.addSection('section1D');

// save sections of this grade level as constants
const section1A = grade1.sections.find(section => section.name === "section1A");
const section1B = grade1.sections.find(section => section.name === "section1B");
const section1C = grade1.sections.find(section => section.name === "section1C");
const section1D = grade1.sections.find(section => section.name === "section1D");

// populate the sections with students
section1A.addStudent('John', 'john@mail.com', [89, 84, 78, 88]);
section1A.addStudent('Joe', 'joe@mail.com', [78, 82, 79, 85]);
section1A.addStudent('Jane', 'jane@mail.com', [87, 89, 91, 93]);
section1A.addStudent('Jessie', 'jessie@mail.com', [91, 89, 92, 93]);

section1B.addStudent('Jeremy', 'jeremy@mail.com', [85, 82, 83, 89]);
section1B.addStudent('Johnny', 'johnny@mail.com', [82, 86, 77, 88]);
section1B.addStudent('Jerome', 'jerome@mail.com', [89, 85, 92, 91]);
section1B.addStudent('Janine', 'janine@mail.com', [90, 87, 94, 91]);

section1C.addStudent('Faith', 'faith@mail.com', [87, 85, 88, 91]);
section1C.addStudent('Hope', 'hope@mail.com', [85, 87, 84, 89]);
section1C.addStudent('Love', 'love@mail.com', [91, 87, 90, 88]);
section1C.addStudent('Joy', 'joy@mail.com', [92, 86, 90, 89]);

section1D.addStudent('Eddie', 'eddie@mail.com', [85, 87, 86, 92]);
section1D.addStudent('Ellen', 'ellen@mail.com', [88, 84, 86, 90]);
section1D.addStudent('Edgar', 'edgar@mail.com', [90, 89, 92, 86]);
section1D.addStudent('Eileen', 'eileen@mail.com', [90, 88, 93, 84]);